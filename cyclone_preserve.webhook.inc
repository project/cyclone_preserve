<?php
/**
 * @file
 * cyclone_preserve.webhook.inc
 */

/**
 * Implements hook_webhook_default_config().
 */
function cyclone_preserve_webhook_default_config() {
  $export = array();

  $webhook = new stdClass();
  $webhook->disabled = FALSE; /* Edit this to true to make a default webhook disabled initially */
  $webhook->api_version = 1;
  $webhook->whid = '2';
  $webhook->title = 'Preserve notifiction';
  $webhook->machine_name = 'cyclone_notification_preserve';
  $webhook->description = 'Webhook for notification of preservation of site.';
  $webhook->unserializer = 'urlencoded';
  $webhook->processor = 'cyclone_preserve_processor_preserve';
  $webhook->config = '';
  $webhook->enabled = TRUE;
  $export['cyclone_notification_preserve'] = $webhook;

  return $export;
}
