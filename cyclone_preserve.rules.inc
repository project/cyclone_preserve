<?php
/**
 * @file
 * Code for Cyclone Preserve rules.
 */

/**
 * Implementation of hook_rules_event_info().
 */
function cyclone_preserve_rules_event_info() {
  return array(
    'cyclone_preserve_preserve_notification' => array(
      'label' => t('A Preserve site notification occurred.'),
      'group' => t('Cyclone'),
      'module' => 'cyclone_preserve',
      'variables' => array(
        'cyc_site' => array('type' => 'node', 'label' => t('The site being notified.')),
        'cyc_notification' => array('type' => 'field_collection_item', 'label' => t('The notification to the site.')),
      ),
    ),
  );
}
