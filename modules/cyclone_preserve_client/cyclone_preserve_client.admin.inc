<?php

/**
 * @file
 * Administration page callbacks.
 */

/**
 * Callback for the settings form.
 */
function cyclone_preserve_client_settings($form, &$form_state) {
  $form['cyclone_preserve_client_site_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Site ID'),
    '#description' => t('The site ID on the originating server.'),
    '#default_value' => variable_get('cyclone_preserve_client_site_id', ''),
  );
  $form['cyclone_preserve_client_originating_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Originating URL'),
    '#description' => t('The site URL on the originating server.'),
    '#default_value' => variable_get('cyclone_preserve_client_originating_url', ''),
  );
  $form['cyclone_preserve_client_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Token'),
    '#description' => t('The token for the site.'),
    '#default_value' => variable_get('cyclone_preserve_client_token', ''),
  );
  $form['cyclone_preserve_client_expiry'] = array(
    '#type' => 'textfield',
    '#title' => t('Expiry'),
    '#description' => t('The expiry date for the site.'),
    '#default_value' => variable_get('cyclone_preserve_client_expiry', ''),
  );
  $form['cyclone_preserve_client_notify_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Notify URL'),
    '#description' => t('The notification URL for the originating server.'),
    '#default_value' => variable_get('cyclone_preserve_client_notify_url', ''),
  );
  $form['cyclone_preserve_client_preserved'] = array(
    '#type' => 'checkbox',
    '#title' => t('Preserved'),
    '#description' => t('Indicates whether the site has been preserved.'),
    '#default_value' => variable_get('cyclone_preserve_client_preserved', ''),
  );
  return system_settings_form($form);
}
