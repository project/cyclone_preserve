<?php

/**
 * @file
 * Utility function for notifications.
 */


/**
 * Send a notification to the originating site.
 *
 * This function will be used to notify the originating site, hence the need for
 * the notify_url to be specified. An example would be the
 * cyclone_tracker_client module sending a preserve message.
 *
 * @param string $notify_url
 *   The URL the notification will be sent to.
 *
 * @param string $type
 *   The notification type.
 *
 * @param string $site_id
 *   The ID of the site the notification is for.
 *
 * @param string $token
 *   The private token used to create the signature.
 *
 * @param string $title
 *   Human readable title for the notification.
 *
 * @param string $build_title
 *   The build title, if any.
 *
 * @param string $build_url
 *   The build URL, if any.
 *
 * @param array $data
 *   The payload, if any.
 *
 * @throws Exception
 *   Throws errors if there is a problem connecting.
 */
function cyclone_preserve_client_send_notification($notify_url, $type, $site_id, $token, $title, $build_title = '', $build_url = '', $data = array()) {
  $payload = array(
    'type' => $type,
    'site_id' => $site_id,
    'title' => $title,
    'build_title' => $build_title,
    'build_url' => $build_url,
    'data' => json_encode($data),
  );
  $signature = cyclone_preserve_client_signature($payload, $token);
  $payload['signature'] = $signature;
  $payload['form_id'] = 'cyclone_notify_form';
  $escaped_payload = http_build_query($payload);
  // Send it.
  $options = array(
    'method' => 'POST',
    'data' => $escaped_payload,
    'timeout' => 15,
    'headers' => array('Content-Type' => 'application/x-www-form-urlencoded'),
  );
  $result = drupal_http_request($notify_url, $options);
  // Check result and throw errors if required.
  if (!empty($result->error)) {
    $vars = array('%error' => $result->error);
    throw new Exception(t('Request error, %error', $vars));
  }
  elseif (!in_array($result->code, array(200, 201))) {
    if (!empty($result->status_message)) {
      $vars = array('%code' => $result->status_message, '%message' => $result->status_message);
      throw new Exception(t('Bad response, %code: %message', $vars));
    }
    else {
      $vars = array('%code' => $result->code);
      throw new Exception(t('Bad response code, %code.', $vars));
    }
  }
}

/**
 * Returns a signature for a name value array of values.
 *
 * The $payload is sorted alphabetically by name, to get a canonical
 * representation before being concatenated and hashed.
 *
 * @param array $payload
 *   Associative array of name value pairs.
 *
 * @param string $secret
 *   The secret shared with the remote server accessing the notification
 *   service.
 *
 * @return string
 *   The hash representing the signature.
 */
function cyclone_preserve_client_signature($payload, $secret) {
  return md5(cyclone_preserve_client_signature_candidate($payload) . ';' . $secret);
}

/**
 * Helper which builds the string to be hashed.
 *
 * Helpful for use in logging payloads with problem signatures.
 */
function cyclone_preserve_client_signature_candidate($payload) {
  ksort($payload);
  $parts = array();
  foreach ($payload as $name => $value) {
    $parts[] = "$name:$value";
  }
  return implode(';', $parts);
}
