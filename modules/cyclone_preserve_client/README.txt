Cyclone Preserve Client Drupal Module
=====================================

Cyclone Preserve Client knows of the existence of certain site variables which
vae been injected by Cyclone Preserve. It makes use of these variables by
providing the user of the site with useful information:
- time until the site expires
- a way to "preserve" the site from the site destruction process on expiry.

This module must be enabled on the newly created site. It does not run on the same
server as cyclone_preserve.

Installation
------------
Cyclone Preserve Client is packaged as a sub module of the Cyclone Preserve
project. It doesn't have any dependencies on cyclone or cyclone_preserve and as
such is relatively lightweight. It does require jquery_countdown to display one
of the blocks.

Enable it on the modules (admin/build/modules) page.

Basic usage
-----------

Place the blocks in an obvious spot on every page, such as the footer. If a site
is about to expire the owner can click the preserve button.

This module include a jQuery countdown widget and as such, may require a little
bit of theming to work in with the site design.

Maintainers
-----------
- Murray Woodman
- Marji Cermak
- Ivan Zugec
