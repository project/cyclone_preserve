<?php

/**
 * @file
 * CyclonePreservePreserveProcessor plugin class for processing preserve notifications.
 */

/**
 * Webhook CyclonePreservePreserveProcessor class.
 */
class CyclonePreservePreserveProcessor {

  /**
   * Configuration form.
   */
  public function config_form() {
    // No configuration needed.
    return array();
  }

  /**
   * Processes data.
   *
   * The $data is an array for urlencoded.
   */
  public function process($data) {
    // The notify inc file has somm helpful functions for us
    module_load_include('inc', 'cyclone', 'cyclone.notify');

    // Process notification
    try {
      list($site, $notification) = cyclone_process_notification('cyclone_preserve_processor_preserve', $data);
    }
    catch (Exception $e) {
      cyclone_notify_out($e->getMessage());
    }

    // Process preserve
    // Remove the expiry date from the site.
    $lang = $site->language;
    if (!empty($site->field_cyc_expiry[$lang][0])) {
      unset($site->field_cyc_expiry[$lang][0]);
      node_save($site);
    }
    // Invoke Rules event.
    if (module_exists('rules')) {
      rules_invoke_event('cyclone_preserve_preserve_notification', $site, $notification);
    }

    // Everything is OK. We return a 201 to show that the notification has been
    // created.
    $message = 'Notification creation success: title=%title, site=%name, site_id=%site_id, type=%type';
    $vars = array(
      '%host' => $host,
      '%name' => $site->title,
      '%site_id' => $site->nid,
      '%type' => $notification->field_cyc_notification_type[$lang][0]['value'],
      '%title' => $notification->field_cyc_notification_title[$lang][0]['value'],
    );
    watchdog('cyclone_notification_preserve', $message, $vars, WATCHDOG_INFO);
    cyclone_notify_out(t($message, $vars), '200 OK');

  }

}
