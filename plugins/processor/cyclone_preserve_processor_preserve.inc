<?php

/**
 * @file
 * Webhook Processor to act on preserve notifications.
 */

$plugin = array(
  'title' => t('Cyclone Preserve Processor'),
  'handler' => array(
    'class' => 'CyclonePreservePreserveProcessor',
  ),
);
