Cyclone Preserve Drupal Module
==============================

Cyclone Preserve injects the site_id and other important site variables into the
"Variables" extension data structure. The variables are then drush vset into the
created site by the Builder. The Builder must implemement
cyclone.builders.Variables in order for this to work.

This module works hand in hand with the Cyclone Preserve Client module which
knows the site variables which have been set.

Installation
------------
Cyclone Preserve is packaged as a sub module of the Cyclone project.
Enable it on the modules (admin/build/modules) page.

Basic usage
-----------

This is set and forget.

Maintainers
-----------
- Murray Woodman
- Marji Cermak
- Ivan Zugec
